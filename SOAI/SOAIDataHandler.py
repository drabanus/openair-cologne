import pandas as pd
import glob
import logging
import json

logger = logging.getLogger()

## Class which handles the loading of data
class SOAIDataHandler:

    def __init__(self):
        pass

    ## Load data from OpenAir Cologne
    #
    # @param path Path to folder with files
    # @param selectValidData Boolean if only valid data shall be loaded
    # @returns Pandas data frame with measurment values depending on time
    def fGetOpenAir(self, path="./data/openair/", selectValidData=True):
        logger.debug(f"Load OpenAir Cologne data from {path}.")

        # Since the measurments are saved in multiple files, use a glob-string and wildcards in order to load the data
        filesOpenAir = glob.glob(path+"*.parquet")
        listOpenAir = []
        for filename in filesOpenAir:
            logger.debug(f"\t- Load {filename}")
            df = pd.read_parquet(filename).assign(feed=lambda d: d["feed"].str.split('-').map(lambda x: x[0]))
            listOpenAir.append(df)

        # Sort the values depending on time
        dataOpenAir = pd.concat(listOpenAir)
        dataOpenAir = dataOpenAir.sort_values("timestamp").reset_index(drop=True)

        # If set to True unvalid data will be discarded
        if selectValidData:
            logger.info("Select only valid data with query {hum <= 100 and r1!=-1 and r2!=-1}")
            dataOpenAir = dataOpenAir.query("hum <= 100 and r1!=-1 and r2!=-1")
            dataOpenAir = dataOpenAir.reset_index(drop=True)

        return dataOpenAir

    ## Load properties of OpenAir Cologn sensors
    #
    # @param pathToFile Path to file where sensor data is saved
    # @param selectValidData Boolean if only valid sensor data shall be loaded
    # @returns Pandas data frame with location of the sensors
    def fGetOpenAirSensors(self, pathToFile="./data/openair/sensors.json", selectValidData=True):
        logger.debug(f"Load open air sensor properties from {pathToFile}.")

        # Define blacklist
        blacklist = ['807f2c96', '807f4d84', '807f4f46', '807f5dba', '807f21ce', '807f239a', '807f273c', '807f480c', '807f2566', '807f3056', '807f7084']
        if selectValidData is True:
            logger.info(f"Select only valid data with is not on blacklist {blacklist}")

        # Open json data where properties of senesors are encoded
        with open(pathToFile) as f:
            data = json.load(f)

        # Create a data frame with location information about the sensors
        df = pd.DataFrame(columns=["feed", "lon", "lat"])
        for feed in data["features"]:
            lonCoordinate = feed["geometry"]["coordinates"][0]
            latCoordinate = feed["geometry"]["coordinates"][1]
            mqtt_id = feed["properties"]["mqtt_id"].split("-")[0]

            if selectValidData is True and mqtt_id in blacklist:
                continue

            df = df.append({"feed": mqtt_id, "lon": lonCoordinate, "lat": latCoordinate}, ignore_index=True)

        return df

    ## Load data from Luftdaten.info
    #
    # @param path Path to folder with files
    # @param selectValidData Boolean if only valid data shall be loaded
    # @returns Pandas data frame with measurment values depending on time
    def fGetLanuv(self, path= "./data/lanuv/", selectValidData=False):
        logger.debug(f"Load Lanuv data from {path}.")

        # Since the measurments are saved in multiple files, use a glob-string and wildcards in order to load the data
        filesLanuv = glob.glob(path+"*.parquet")
        listLanuv = []
        for filename in filesLanuv:
            logger.debug(f"\t- Load {filename}")
            df = pd.read_parquet(filename)
            listLanuv.append(df)

        # Sort values depending on time
        dataLanuv = pd.concat(listLanuv, ignore_index=True)
        dataLanuv = dataLanuv.sort_values("timestamp").reset_index(drop=True)

        if selectValidData is True:
            logger.warning("Select valid data is set to true, but no rules for valid data are given.")

        return dataLanuv

    ## Load properties of OpenAir Cologn sensors
    #
    # @param pathToFile Path to file where sensor data is saved
    # @returns Pandas data frame with location of the sensors
    def fGetLanuvSensors(self, pathToFile="./data/lanuv/sensors.csv"):
        logger.debug(f"Load lanuv sensor properties from {pathToFile}.")

        df = pd.read_csv(pathToFile, sep=";")

        return df

