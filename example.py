from SOAI.SOAIDataHandler import SOAIDataHandler
import logging

logger = logging.getLogger()

dataHandler = SOAIDataHandler()

dataOpenAir = dataHandler.fGetOpenAir("./data/openair/", selectValidData=True)
dataOpenAirSensors = dataHandler.fGetOpenAirSensors(selectValidData=True)

dataLanuv = dataHandler.fGetLanuv("./data/lanuv/")
dataLanuvSensors = dataHandler.fGetLanuvSensors()

logger.debug("Lanuv and OpenAirData is available")
