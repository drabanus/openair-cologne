# OpenAir Cologne

### School of AI Cologne project in cooperation with OpenAir Cologne.

-------------------

## Run the Code

In order to run the project perform the following steps:
- Make sure you have *git* installed
- Clone the repository with **git clone https://gitlab.com/N.Stausberg/openair-cologne**
- Change to a unique branch with **git checkout -b someName**
- Inside the downloaded repository create logging folder **mkdir logs**
- Install all packages in *requirements.txt* e.g in some virtual enviroment
- Run **python example.py**

------------------

## Informations about the sensors

The following data is available:
- **feed** - sensor id
- **hum** - humidity
- **temp** - temperature
- **pm10** and **pm25** - measurments of particulate matter
- **r1** and **r2** - measurments of NO2
- **rssi** - ?

Following issues are known:
- Sometime very high **hum** values can ocurre. Most likely due to hardware issues of the sensors.
- No measurment values for **pm10** and **pm25** since no sensors for particulate matter are available.
- High temperature values since the sensor misses a proper calibration, but it should be at least proportional to the real temperature.
- The resistor used for measurment value **r1** is wrong. Therefore **r2** is more reliable.
