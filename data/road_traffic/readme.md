# OpenAir Cologne

### School of AI Cologne project in cooperation with OpenAir Cologne.

-------------------

## Run the Code

In order to run the project perform the following steps:
- Make sure you have *git* installed
- Clone the repository with **git clone https://gitlab.com/N.Stausberg/openair-cologne**
- Change to a unique branch with **git checkout -b someName**
- Inside the downloaded repository create logging folder **mkdir logs**
- Install all packages in *requirements.txt* e.g in some virtual enviroment
- Run **python example.py**

------------------

## Informations about the sensors

The following data is available:
- **feed** - sensor id
- **hum** - humidity
- **temp** - temperature
- **pm10** and **pm25** - measurments of particulate matter
- **r1** and **r2** - measurments of NO2
- **rssi** - ?

Following issues are known:
- Sometime very high **hum** values can ocurre. Most likely due to hardware issues of the sensors.
- No measurment values for **pm10** and **pm25** since no sensors for particulate matter are available.
- High temperature values since the sensor misses a proper calibration, but it should be at least proportional to the real temperature.
- The resistor used for measurment value **r1** is wrong. Therefore **r2** is more reliable.
MacBook-Pro:openair-cologne drabanus$ cd data/
MacBook-Pro:data drabanus$ cat README.md 
cat: README.md: No such file or directory
MacBook-Pro:data drabanus$ cat road_traffic/README.md 
**Director structure and file naming convention**

The area of Cologne has been covered with 9 tiles with zoom level "15".

The file naming scheme is as follows:

YYMMSDDhhmmss_LOC_IJXY.png

where

YY: two-digit year,

MM: month,

DD: day,

hh: hour in 24h-notation,

mm: minutes,

ss: seconds,

LOC: town name (here CGN),

I: index for the tile's rows (1-3),

J: index for the tile'S columns (1-3)

X: "W", "C", or "E" for west, center or east tile column,

Y: "N", "C", or "S" for north, center or south row.


`|  11WN  |  12CN  |  13EN  |`


`----------------------------`

`|  21WC  |  22CC  |  23EC  |`

`----------------------------`

`|  31WS  |  32CS  |  33ES  |`